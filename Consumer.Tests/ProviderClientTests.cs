﻿using System.Net;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Text.Json.Serialization;
using FluentAssertions;
using Moq;
using PactNet;
using PactNet.Output.Xunit;
using Xunit.Abstractions;
using Match = PactNet.Matchers.Match;

namespace Consumer.Tests;

public class ProviderClientTests
{
    private readonly IPactBuilderV4 pact;
    private readonly Mock<IHttpClientFactory> _mockFactory;
    public ProviderClientTests(ITestOutputHelper output)
    {
        _mockFactory = new Mock<IHttpClientFactory>();

        var config = new PactConfig()
        {
            PactDir = "../../../pacts",
            Outputters = new[] { new XunitOutput(output) },
            DefaultJsonSettings = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                PropertyNameCaseInsensitive = true,
                Converters = { new JsonStringEnumConverter() }
            },
            LogLevel = PactLogLevel.Debug
        };

        pact = Pact.V4("Consumer API", "Provider API", config).WithHttpInteractions();
    }

    [Fact]
    public async Task GetPerson()
    {
        var expected = new Person("John", "Doe", 42);

        this.pact
            .UponReceiving("a request for an person")
                .WithRequest(HttpMethod.Get, "/person")
                .WithHeader("Accept", "application/json")
            .WillRespond()
                .WithStatus(HttpStatusCode.OK)
                .WithJsonBody(new
                {
                    Age = Match.Integer(expected.Age),
                    FirstName = Match.Equality(expected.FirstName),
                    LastName = Match.Equality(expected.LastName)
                });

        await this.pact.VerifyAsync(async ctx =>
        {
            this._mockFactory
                .Setup(f => f.CreateClient("Persons"))
                .Returns(() => new HttpClient
                {
                    BaseAddress = ctx.MockServerUri,
                    DefaultRequestHeaders =
                    {
                        Accept = { MediaTypeWithQualityHeaderValue.Parse("application/json") }
                    }
                });
            var client = new ProviderHttpClient(this._mockFactory.Object);
            
            var result = await client.GetPerson();

            result.Should().Be(expected);
        });
    }
}