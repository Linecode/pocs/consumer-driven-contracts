﻿using PactNet;
using PactNet.Output.Xunit;
using PactNet.Verifier;
using Xunit.Abstractions;

namespace Provider.Tests;

public class ProviderTests : IClassFixture<MinimalApiFixture>, IDisposable
{
    private readonly MinimalApiFixture _fixture;
    private readonly PactVerifier _verifier;

    public ProviderTests(ITestOutputHelper output, MinimalApiFixture fixture)
    {
        _fixture = fixture;
        _verifier = new PactVerifier("Provider API", new PactVerifierConfig
        {
            LogLevel = PactLogLevel.Debug,
            Outputters = new []
            {
                new XunitOutput(output)
            }
        });
    }
    
    public void Dispose()
    {
        _verifier.Dispose();
    }
    
    [Fact]
    public void Verify()
    {
        string pactPath = Path.Combine("..",
            "..",
            "..",
            "..",
            "Consumer.Tests",
            "pacts",
            "Consumer API-Provider API.json");

        _verifier
            .WithHttpEndpoint(_fixture.BaseApiAddress)
            .WithFileSource(new FileInfo(pactPath))
            .Verify();
    }
}