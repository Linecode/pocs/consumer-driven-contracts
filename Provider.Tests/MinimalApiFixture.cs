﻿using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Xml;

namespace Provider.Tests;

// Minimal API have some problems with pact io, so we need to start process manually
// https://github.com/pact-foundation/pact-net/issues/444
public class MinimalApiFixture : IDisposable
{
    private readonly Process _process;

    public readonly Uri BaseApiAddress;

    public MinimalApiFixture()
    {
        var assembly = typeof(MinimalApiFixture).Assembly;
        var pathToCsprojFile = Path.Combine(assembly.Location.Replace($"{assembly.GetName().Name}.dll", ""), "..", "..", "..", $"{assembly.GetName().Name}.csproj");
        var pathToProject = GetApiProjectPath(pathToCsprojFile);

        BaseApiAddress = new Uri(GetApiAddress());
        
        _process = new Process
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = "dotnet",
                Arguments = $"run --project {pathToProject} --urls {BaseApiAddress}",
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                CreateNoWindow = true
            }
        };

        _process.Start();
    }
    
    private static string GetApiProjectPath(string csprojPath)
    {
        XmlDocument doc = new XmlDocument();
        doc.Load(csprojPath);

        // Select project references
        XmlNodeList projectRefs = doc.GetElementsByTagName("ProjectReference");

        foreach (XmlNode refNode in projectRefs)
        {
            // Assuming the reference includes the path as an attribute
            if (refNode.Attributes != null)
            {
                if (refNode.Attributes["PactServer"] == null) continue;
                
                var refPath = refNode.Attributes["Include"]?.Value;
                // Get the absolute path if the path is relative
                var fullPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(csprojPath) ?? string.Empty, refPath ?? string.Empty));
                return fullPath;
            }
        }
        
        throw new Exception("Can't find path to project");
    }
    
    private static ushort GetNextTcpPort()
    {
        using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
        {
            socket.Bind(new IPEndPoint(IPAddress.Loopback, 0));
            return (ushort)(((IPEndPoint)socket.LocalEndPoint!)).Port;
        }
    }

    private static string GetApiAddress() => $"http://localhost:{GetNextTcpPort()}";


    public void Dispose()
    {
        _process.Kill();
    }
}