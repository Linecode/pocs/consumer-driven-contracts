﻿namespace Consumer;

public class ProviderHttpClient : IProviderHttpClient
{
    private readonly IHttpClientFactory _httpClientFactory;

    public ProviderHttpClient(IHttpClientFactory httpClientFactory)
    {
        _httpClientFactory = httpClientFactory;
    }

    public async ValueTask<Person?> GetPerson()
    {
        using var client = _httpClientFactory.CreateClient("Persons");
        return await client.GetFromJsonAsync<Person>("person");
    }
}

public interface IProviderHttpClient
{
    ValueTask<Person?> GetPerson();
}

public record Person(string FirstName, string LastName, int Age);